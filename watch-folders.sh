#!/usr/bin/env bash

SERVICE_ID=146
SOURCE_FOLDER="source"
SOURCE_LOCALE="en-us"
CURRENCY=US

job_ids=()

shopt -s expand_aliases
alias ltrans="java -jar lib/liox-content-cli.jar"

echo
echo "Checking '$SOURCE_FOLDER' for files to translate..."

for dir in "$SOURCE_FOLDER"/*
do
  echo
  echo "- Checking '$dir' for files to translate..."

  LOCALES=`echo $dir | cut -d \/ -f 2`
  SOURCE_LOCALE=`echo $LOCALES | cut -d \_ -f 1`
  TARGET_LOCALE=`echo $LOCALES | cut -d \_ -f 2`

  for LOCAL_FILE in "$dir"/*
  do

    if [ -e "$LOCAL_FILE" ]
    then
      # make sure there's a file here
      echo "- Found '$LOCAL_FILE'..."
      echo "- Adding file '$LOCAL_FILE'..."
      FILE_ID=`ltrans file add --local-file=$LOCAL_FILE --source-locale=$SOURCE_LOCALE`

      if [ ! $? -eq 0 ]; then
        echo "Adding file failed: $FILE_ID"
        exit $?
      else
        echo "File uploaded with ID $FILE_ID"
      fi

      echo "- Creating job to translate '$LOCAL_FILE' from '$SOURCE_LOCALE' to '$TARGET_LOCALE'..."
      JOB_ID=`ltrans job add --source-files=$FILE_ID --source-locale=$SOURCE_LOCALE --target-locales=$TARGET_LOCALE --service=$SERVICE_ID`

      if [ ! $? -eq 0 ]; then
        echo "Creating job for File ID $FILE_ID failed: $JOB_ID"
        exit $?
      else
        echo "- Job created with ID $JOB_ID"
        job_ids+=("$JOB_ID")

        rm $LOCAL_FILE
      fi
    else
      echo "- No files found in ${dir}"
    fi

  done
done

JOBS=`(IFS=,; echo "${job_ids[*]}")`

echo "$JOBS"
echo "${#job_ids[@]}"

if [ ${#job_ids[@]} -ne 0 ]
then
  echo
  echo "The following JOB_IDS were created: $JOBS"

  QUOTE_ID=`ltrans quote add --jobs=$JOBS --currency=$CURRENCY`

  if [ ! $? -eq 0 ]; then
    echo "Requesting quote for JOB ID $JOB_ID failed: $QUOTE_ID"
    exit $?
  else
    echo "Quote requested with ID $QUOTE_ID"
    echo "$QUOTE_ID" >> data/quotes.txt
  fi
else
  echo
  echo "No jobs created."
fi
