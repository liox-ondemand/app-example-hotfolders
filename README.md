# onDemand CLI - Hot Folder Tutorial

This project contains a pair of scripts for watching, submitting and downloading a folder of files to be translated.

## Getting Started

To see how these scripts work, do the following:

1. Check out the project.
1. Set up your Environment Variables.
1. Copy `files/example.txt` into the two `source` sub-directories.
1. Run `watch_folder.sh`.
1. Go to the site and pay for your work.
1. Wait for the jobs to complete.
1. Run `watch-quotes.sh` to download the results.

Here is an example run of the above:

```bash
$ git clone git@bitbucket.org:liox-ondemand/app-example-hotfolders.git
$ cd app-example-hotolders
$ export LIOX_API_ACCESS_KEY_ID=<your-sandbox-api-key>
$ export LIOX_API_SECRET_KEY=<your-sandbox-api-secret>
$ export LIOX_API_ENDPOINT=https://developer-sandbox.liondemand.com/api
$ export LIOX_API_DEFAULT_CURRENCY=USD
$ cp files/example.txt source/en-us_de-de/
$ cp files/example.txt source/en-us_fr-fr/
$ ./watch_folder.sh
$ # Pay for Quote
$ ./watch_quotes.sh
```

## The Folders

This example project using the following directory structure:

- `data/quotes.txt` - This file stores the IDs of Quotes that have been requested for translation Jobs.
- `files/example.txt` - This is an example source file provided for your convenience.
- `source`
  - `en-us_de-de` - This is where you place any files you want translated from English to German.
  - `en-us_fr-fr` - This is where you place any files you want translated from English to French.
- target
  - `en-us_de-de` - Files translated from English to German will be placed here by the `watch-quotes.sh` script.
  - `en-us_fr-fr` - Files translated from English to French will be placed here by the `watch-quotes.sh` script.

## The Scripts

### watch-folders.sh

This script watches the `source` folder for new files to be translated.

When it encounters them it does the following:

- Uploads the file to the onDemand API
- Creates a translation Job for the file
- Generates a Quote for the translation Job
- Stores the pending Quote IDs in `data/quotes.txt`

### watch-quotes.sh

This script watches the Quotes generated by `watch-folders.sh` and stored in `data/quotes.txt`.

If a Quote is waiting to be authorized, it authorizes the quote.

If a Quote is in the process of being complete, it does nothing.

If a Quote is completed, it will:

- downloads the translated file
- stores it in the `target` directory
- removes the Quote ID from `data/queue.txt`
